import React from 'react'
import {IProp} from './type'
import './style.css'

function Navbar(iprop: IProp){
  
    
    
    return(
        <nav className="nav-style">
            <a href="/about">About</a>
            <a href="/user">User</a>
            <a href="/posts">Post</a>
        </nav>
    )
}
export default Navbar