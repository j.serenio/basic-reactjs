export interface IProp {}

export interface IState {
    userList: User[]
}

export interface User {
    id: string;
    name: string;
    _id: string;
    email: string;
    password: string;
    role: string;
}
