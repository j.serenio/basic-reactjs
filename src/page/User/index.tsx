import axios from "axios";
import {IProp, IState} from "./type";
import { useEffect, useState } from "react";
import './style.css'
import { useNavigate } from "react-router-dom";

function Users(props: IProp) {
    const [userList, setUserList] = useState <IState["userList"]>([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [role, setRole] = useState("Intern");
    const [password, setPassword] = useState("")
    const nav = useNavigate();
   

    useEffect(() => {
        handleRequestUsers();
    }, []);




const handleRequestUsers = async () => {
    try{
        const response = await axios.get(
            "http://localhost:5173/api/user"
        );
        setUserList(response.data);
    }catch (error){
        alert('Error')
    }
    
};
{/*
const addUSERbutton = (id = String) => {
    try{
        const response = await axios.post(
            "http://localhost:5173/api/user", 
        );
     }

        catch (error){
            alert('Error')
        }

}
*/}


const handleUserchange = (event: any) => {
    const {name, value} = event.target
    switch(name){
        case 'name':
            return setName(value);
        case 'email':
            return setEmail(value);
        case 'password':
            return setPassword(value);
        case 'role':
            return setRole(value);
    }

}

const addUSERbutton = async () =>  {
    if(!name || !email || !password )
    {
        alert("Enter Required fields")
    }
    else{

    
    try{
        const response = await axios.post(
            "http://localhost:5173/api/user", {name, email, password, role}
        );
        console.log(response);
        if(response.status == 200){ //reset
            handleRequestUsers();
            setName("");
            setEmail("");
            setPassword("");
            setRole("INTERN");
        }
    }  catch (error){
        alert('Error')
    }
}
}

const handleDeleteUser = async () => {
  
    try{
        const response = await axios.delete(
            "http://localhost:5173/api/user"
        );
        setUserList(response.data)
       
    }catch (error){
        alert('Error')
    }
    
}

    const handleLogout = () => {
        localStorage.removeItem("Token")
        nav("/");
    }



return(
    <div className ="flexmain">
        <div className="firstflex">
            <label>Full Name</label>
            <input type ="text" name="name" value ={name} onChange={handleUserchange}></input>
            <label>Email</label>
            <input type ="text" name="email" value={email} onChange={handleUserchange}></input>
            <label>Password</label>
            <input type="password" name="password" value={password} onChange={handleUserchange}></input>
            <label>Role</label>
            <select value={role} name="role" onChange={handleUserchange}>
                <option value="ADMIN">
                    Admin
                </option>
                <option value="LEAD">
                    Lead
                </option>
                <option value="DEVELOPER">
                    Developer
                </option>
            </select>
            <button onClick={()=>addUSERbutton()}>Add New User</button>
        </div>
        <div className="secondFlex">
        <button onClick={handleLogout}>logout</button>
        <table border={1}>
            <tbody>
                <tr>
                   <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                {userList.map((items) => {
                        return (
                            <tr key={items.id}>
                                <td>{items._id}</td>
                                <td>{items.name}</td>
                                <td>{items.email}</td>
                                <td>{items.password}</td>
                                <td>{items.role}</td>
                                <td><button onClick={handleDeleteUser}>Delete</button></td>
                            </tr>
                        )
                    })}
            </tbody>
        </table>
        </div>
    </div>
)
}

export default Users