import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { IProp } from "./type";


import { accounts } from "../../helper/AuthenticatedAccount";

function Login(props: IProp) {
  const [showPass, setShowPass] = useState<boolean>(false);
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();

  const handleInputUser = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setUsername(value);
  };
  const handleInputPass = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setPassword(value);
  };

  const handleLoginButton = () => {
    const token = {
      username,
      password,
    };

    const authenticated = accounts
      .map((item) => JSON.stringify(item))
      .includes(JSON.stringify(token));
    if (authenticated) {
      localStorage.setItem("Token", JSON.stringify(token));
      navigate("/");
    } else {
      alert("Invalid Account");
    }
  };
  return (
    <div>
      <h1>Login</h1>
      <div>
        <div>
          <span>Username: </span>
          <input type="text" onChange={handleInputUser} value={username} />
        </div>
        <div>
          <span>Password: </span>
          <input
            type={showPass ? "text" : "password"}
            onChange={handleInputPass}
            value={password}
          />
        </div>
        <input
          type="checkbox"
          checked={showPass}
          onChange={() => setShowPass((prev) => !prev)}
        />{" "}
        <span>See Password</span> <br />
        <button className="btn" onClick={handleLoginButton}>
          Login
        </button>
      </div>
    </div>
  );
}

export default Login;
