import React, {useEffect, useState} from 'react';
import {IProp, IState} from "./type";
import './style.css'
import axios from 'axios';



function Posts () {
    const [postList, setPostList] = useState<IState["postList"]>([]);
    const [userList, setUserList] = useState<IState["userList"]>([]);
    const [title, setTitle] = useState("");
    const [message, setMessage] = useState("");

    useEffect(() => {
        handleRequestPostList();
    }, [])

const handleRequestPostList = async() => 
{   try {
    const response = await axios.get("http://localhost:5173/api/post");
    setPostList(response.data)

} catch(error) {
    console.log(error)
}

}

useEffect(() => {
    handleRequestUsers();
}, []);




const handleRequestUsers = async () => {
try{
    const response = await axios.get(
        "http://localhost:5173/api/user"
    );
    setUserList(response.data);
}catch (error){
    alert('Error')
}

};

const handlePostchange = (event: any) => {
    const {name, value} = event.target
    switch(name){
        case 'title':
        return setTitle(value);
        case 'message':
        return setMessage(value)
    }

}

const AddPostButton = async () =>
{ try {
    const response = await axios.post("http://localhost:5173/api/post", {title, message});
    console.log(response)
    if(response.status == 200){ //reset
        handleRequestPostList();
        setTitle("");
        setMessage("");
    }
}
catch(error) {
    console.log(error)
}
}


    return (
        <>
        <label>Title</label>
        <input type ="text"   value={title} placeholder='Enter Title' onChange={handlePostchange} name="title"></input>
        <label>Message</label>
        <textarea placeholder ="Enter Message" value={message} onChange={handlePostchange} name="message"></textarea>
        <br></br>
        <button onClick={AddPostButton}>Enter Post</button>
        <br></br>
        <table border={1}>
            <tbody>
                <tr>
                    <th>ID</th>
                    
                    <th>Title</th>
                    <th>Message</th>
                    <th>Action</th>
                   
                </tr>
                
                {postList.map((items) => {
                    
                        return (
                            <tr key={items._id}>
                                <td>{items._id}</td>
                                
                                <td>{items.title}</td>
                                <td>{items.message}</td>
                                <td><button>Update</button><button>Delete</button></td>
                            </tr>
                        )
                    })}
            </tbody>
        </table>
    

        </>
    )
}     
export default Posts
