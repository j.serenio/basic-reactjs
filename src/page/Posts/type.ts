export interface IProp {

}
export interface IState {
    postList: Post[];
}
export interface Post {
    name: string;
    _id: number;
    userId: number;
    title: string;
    message: string;
}
export interface IState {
    userList: User[]
}export interface User{
    id: string;
    name: string;
}