import React, {useState} from "react";
import { IProp, IState, ITodo } from "./type";
import "./style.css";
import {useNavigate} from 'react-router-dom'

import TodoDisplay from "./components/TodoDisplay";
import TodoForm from "./components/TodoForm";

{/* class Home extends React.Component<IProp, IState> {
  constructor(props: IProp) {
  super(props); */}

  const Home: React.FC  = (props: IProp)=> {
    const [state, setState] = useState<IState>({
        temp_input: "",
        temp_message: "",
        input_list: [],
        showTable: false,
        editItem: {
          id: '',
          input: '',
          message: '',
        }
    })
 const nav = useNavigate();



  const inputHandler = (event: any) => {
    const { value } = event.target;
    setState ((prev) => ({ ...prev, temp_input: value }));
  };

  const messageHandler = (event: any) => {
    const { value } = event.target;
    setState((prev) => ({ ...prev, temp_message: value }));
  };

 const btnAddHandler = () => {

  {if(!state.temp_input || !state.temp_message) {
    ( alert("Please fill out all fields"))
        }
        else {
    const { temp_input, temp_message, input_list } = state;
    const newInputList = [
      ...input_list,
      {
        id: `${new Date().getTime() / 1000}`,
        input: temp_input,
        message: temp_message,
      },
    ];
    setState((prev) => ({...prev,
      input_list: newInputList,
      temp_input: "",
      temp_message: "",
    }));
  };
}
 }

  const handleCheckShowTable = (event: any) => {
    const { checked } = event.target;
    setState((prev) => ({...prev, showTable: checked }));
  };

  const handleActionDelete = (id: string) => {
    const { input_list } = state;
    const newInputList = input_list.filter((item) => item.id !== id);
    setState((prev) => ({...prev, input_list: newInputList }));
  };

  const handleEditButton = (item: ITodo) => {
    setState((prev) => ({...prev, editItem: {id: item.id, message: item.message, input: item.input }}));
  };

  const handleEditOnChange = (event: any) => {
    const {name, value} = event.target;
    if(name === "editInput") {
        setState((prev) => ({...prev, editItem: {...state.editItem, input: value}}))
    }
    else if(name === "editMessage") {
      setState((prev) => ({...prev, editItem: {...state.editItem, message: value}}))
    }
  }

  const handeUpdateButton = () => {
    const { editItem, input_list  } = state;

    const newList = input_list.map(items => {
      if(items.id === editItem.id) {
        return editItem
      }
      return items;
    })
    setState((prev) => ({...prev, input_list: newList, editItem: {id: '', input: '', message: ''}}))
  }

  const handleCancelButton = () => {
    console.log("clicked");
    
    setState((prev) => ({...prev,
        editItem: {
          id: '',
          input: '',
          message: ''
        }
      }));
  }

  const handleLogout = () => {
    localStorage.removeItem("Token")
    nav("/");
}

  
    //render or display

    return (
      <div className ="flexx">
        <h1>Home Page</h1>
        <div className="basic-form-container">
          <TodoForm
            btnAddHandler={btnAddHandler}
            inputHandler={inputHandler}
            messageHandler={messageHandler}
            temp_input={state.temp_input}
            temp_message={state.temp_message}
            handleCheckShowTable={handleCheckShowTable}
          />
            
        </div>
        <div className="secondflex">
        <TodoDisplay
          actionEdit={handleEditButton}
          editItem={state.editItem}
          actionDelete={handleActionDelete}
          handleCancelButton={handleCancelButton}
          showTable={state.showTable}
          inputlist={state.input_list}
          handleEditOnChange={handleEditOnChange}
          handeUpdateButton={handeUpdateButton}
        />
        <button className = "btn btn-warning" onClick ={handleLogout}>LOGOUT</button>
        </div>
      </div>
    );

  }


export default Home;
