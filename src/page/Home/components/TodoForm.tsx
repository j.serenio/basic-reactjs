import { ITodoFormProp } from "../type";

function TodoForm(props: ITodoFormProp) {
    const { inputHandler, messageHandler, btnAddHandler, temp_input, temp_message, handleCheckShowTable } = props;
  return (
    <div className ="firstflex">
      <input
        name="title"
        type="text"
        onChange={inputHandler}
        value={temp_input}
      />
      <textarea
        name="message"
        onChange={messageHandler}
        value={temp_message}
      /> 
      <button className="btn" onClick={btnAddHandler}>
        Add
      </button>
      <input type="checkbox" onChange={handleCheckShowTable} />{" "}
    <span>Show Table</span>
    </div>
  );
}

export default TodoForm;
