import "../style.css";
import { ITodoDisplayProp } from "../type";

function TodoDisplay(props: ITodoDisplayProp) {
  const { inputlist, showTable,  editItem, actionDelete, actionEdit, handleEditOnChange, handeUpdateButton, handleCancelButton } = props;

  return (
    <>
      {!showTable ? (
        <></>
      ) : (
        <table border={1}>
          <tbody>
            <tr>
              <th>Title</th>
              <th>Message</th>
              <th>Action</th>
            </tr>
            {inputlist.map((items, index) => {
              if (items.id === editItem?.id && items.id) {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <input name="editInput" type="text" value={editItem.input} onChange={handleEditOnChange}/>
                    </td>
                    <td>
                      <input name="editMessage" type="text" value={editItem.message} onChange={handleEditOnChange}/>
                    </td>
                    <td>
                      <button onClick={handeUpdateButton}>Update</button>
                      <button onClick={handleCancelButton}>Cancel</button>
                    </td>
                  </tr>
                );
              } else {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.input}</span>
                    </td>
                    <td>{items.message}</td>
                    <td>
                      <button
                        className="btn btn-primary"
                        onClick={() => actionEdit(items)}
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => actionDelete(items.id)}
                        className="btn btn-error"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
      )}
    </>
  );
}

export default TodoDisplay;
