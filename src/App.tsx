import "./App.css";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import Home from "./page/Home";
import Login from "./page/Login";
import Register from "./page/Register";
import User from "./page/User"
import Posts from "./page/Posts";
import Navbar from "./components";

import { protectedRoute, isLoggedIn } from "./helper/ProtectedRoute";


const router = createBrowserRouter([

  {
    path: "/",
    element: <Home />,
    loader: protectedRoute
  },
  {
    path: "/login",
    element: <Login />,
    loader: isLoggedIn
  },
  {
    path: "/register",
    element: <Register />,
    loader: protectedRoute
  },
  {
  path: "/user",
  element: <User />,
  },
  {
    path: "/posts",
    element: <Posts />,
    },
  {
    path: "*",
    element: <h1>404 Not found.</h1>
  }
]);

function App() {
  return (<>
    <Navbar />
    <RouterProvider router={router} />
    </>
    );
}

export default App;
